package com.sun;

import com.sun.tools.javac.Main;

/**
 * Description:
 * User: tan
 * DateTime: 2017/10/26 10:14
 */
public class Test {
    public static void main(String[] args) {
        Main m = new Main();
        //  File f = new File("I:\\java-study\\java-base\\src\\main\\java\\com\\study\\javac\\MyClassFileTest.java");
        m.compile(asArray("java-base\\src\\main\\java\\com\\study\\javac\\MyClassFileTest.java"));
    }

    private static <T> T[] asArray(T... args) {
        return args;
    }
}
